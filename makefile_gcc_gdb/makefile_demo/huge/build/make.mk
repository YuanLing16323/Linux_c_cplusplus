
.PHNOY:all clean

MKDIR = mkdir
RM = rm
RMFLAGS = -rf

CC = gcc
AR = ar
ARFLAGS = crs

DIR_OBJS = objs
DIR_DEPS = deps
DIR_EXES = ${ROOT}/build/exes
DIR_LIBS = ${ROOT}/build/libs
DIRS = ${DIR_OBJS} ${DIR_EXES} ${DIR_DEPS} ${DIR_LIBS}

ifneq ("${EXE}", "")
EXE := ${addprefix ${DIR_EXES}/, ${EXE}}
RMS += ${EXE}
endif

ifneq ("${LIB}", "")
LIB := ${addprefix ${DIR_LIBS}/, ${LIB}}
RMS += ${LIB}
endif

SRCS = ${wildcard *.c}

OBJS = ${SRCS:.c=.o}
OBJS := ${addprefix ${DIR_OBJS}/, ${OBJS}}

DEPS = ${SRCS:.c=.dep}
DEPS := ${addprefix ${DIR_DEPS}/, ${DEPS}}

ifeq ("${wildcard ${DIR_OBJS}}", "")
DEP_DIR_OBJS := ${DIR_OBJS}
endif

ifeq ("${wildcard ${DIR_EXES}}", "")
DEP_DIR_EXES := ${DIR_EXES}
endif

ifeq ("${wildcard ${DIR_DEPS}}", "")
DEP_DIR_DEPS := ${DIR_DEPS}
endif

ifeq ("${wildcard ${DIR_LIBS}}", "")
DEP_DIR_LIBS := ${DIR_LIBS}
endif

# 在各个子模块内定义INCLUDE_DIRS
ifneq (${INCLUDE_DIRS}, "")
INCLUDE_DIRS := ${strip ${INCLUDE_DIRS}}
INCLUDE_DIRS := ${addprefix -I, ${INCLUDE_DIRS}}
endif

ifneq (${LINK_LIBS}, "")
LINK_LIBS := ${strip ${LINK_LIBS}}
LIB_ALL := ${notdir ${wildcard ${DIR_LIBS}/*}}
LIB_FILTERED := ${addsuffix %, ${addprefix lib, ${LINK_LIBS}}}
${eval DEP_LIBS = ${filter ${LIB_FILTERED}, ${LIB_ALL}}}
DEP_LIBS := ${addprefix ${DIR_LIBS}/, ${DEP_LIBS}}
LINK_LIBS := ${addprefix -l, ${LINK_LIBS}}
endif

all:${EXE} ${LIB}

# 使用生成的依赖关系
# 构建目标是clean时，不包含依赖关系
ifneq (${MAKECMDGOALS}, clean)
include ${DEPS}
endif



${DIRS}:
	${MKDIR} $@

${EXE}:${DEP_DIR_EXES} ${OBJS} ${DEP_LIBS}
	${CC} -L${DIR_LIBS} -o $@ ${filter %.o, $^} ${LINK_LIBS}

${LIB}:${DEP_DIR_LIBS} ${OBJS}
	${AR} ${ARFLAGS} $@ ${filter %.o, $^}

${DIR_OBJS}/%.o:${DEP_DIR_OBJS} %.c
	${CC} ${INCLUDE_DIRS} -o $@ -c ${filter %.c, $^}

${DIR_DEPS}/%.dep: ${DEP_DIR_DEPS} %.c
	@echo "Creating $@..."
#   set -e 的作用是告诉shell。再生成依赖关系文件的过程中如果出现任何错误就直接退出。
#   shell异常退出的最终表现就是make会告诉我们出错了，从而停止后续的make工作。如果
#   不进行这一设置。当构建一栏文件出现错误时，make还会继续后面的工作(并最终出错)
	set -e; \
	${RM} ${RMFLAGS} $@.tmp ; \
	${CC} ${INCLUDE_DIRS} -E -MM ${filter %.c, $^} > $@.tmp ; \
	sed 's,\(.*\)\.o[ :]*,objs/\1.o $@: ,g' < $@.tmp > $@ ; \
	${RM} ${RMFLAGS} $@.tmp

clean:
	${RM} ${RMFLAGS} ${RMS}
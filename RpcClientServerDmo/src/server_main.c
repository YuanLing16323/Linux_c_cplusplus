
#include <stdio.h>
#include "server.h"
#include "wifi_hal_crpc_server.h"
#include "log.h"

#define CONFIG_ROOR_DIR "/home/yuanling"

int main(void)
{
    char rpcSockPath[] = CONFIG_ROOR_DIR"/unix_sock.sock";
    if (access(rpcSockPath, 0) == 0) {
        LOG_ERROR("%s:%d rpcSockPatch:%s access = 0 failed\n", __func__, __LINE__, rpcSockPath);
        unlink(rpcSockPath);
    }

    InitLogSystem("rpc_server_log");
    InitRpcFunc();

    RpcServer *server = CreateRpcServer(rpcSockPath);
    if (server == NULL) {
        LOG_ERROR("%s:%d  Create RPC Server by %s failed!\n",  __func__, __LINE__, rpcSockPath);
        return -1;
    }
    LOG_INFO("%s:%d  Create RPC Server Succ, server is runing...", __func__, __LINE__);
    RunRpcLoop(server);
    /* stop wpa_supplicant, hostapd, and other resources */
    // ForceStop();
    ReleaseRpcServer(server);
    DeInitLogSystem();
    return 0;
}
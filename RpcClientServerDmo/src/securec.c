

#ifdef __cplusplus
extern "C" {
#endif

#include "securec.h"
#include <stdarg.h>
#include <stdio.h>
int memset_s(void *dest, size_t destsz, int ch, size_t count) {
    
    if (dest == NULL) {
        return ERROR;
    }

    // 检查destsz和count的值，确保不会发生内存越界
    if (destsz < count) {
        return ERROR; // 内存越界错误
    }

    char *pDest = (char *) dest;
    size_t i;

    // 将ch的值赋给每个字节，直到count为0
    for (i = 0; i < count; i++) {
        *pDest = ch;
        pDest++;
    }

    return EOK; // 初始化成功
}
int memmove_s(void *dest, size_t destsz, const void *src, size_t count) {

    // 参数检查
    if (dest == NULL || src == NULL) {
        if (dest != NULL && destsz > 0) {
            memset_s(dest, destsz, 0, destsz); // 可能清空目标缓冲区
        }
        return ERROR; // 假设错误码
    }
    
    if (destsz > RSIZE_MAX || count > RSIZE_MAX) {
        memset_s(dest, destsz, 0, destsz);
        return ERROR;
    }
    
    if (count > destsz) {
        memset_s(dest, destsz, 0, destsz);
        return ERROR;
    }
    
    // 处理复制
    
    if (dest == src) {
        return EOK; // 相同地址，无需操作
    }
    
    char *d = (char *)dest;
    
    const char *s = (const char *)src;
    
    if (d < s || d >= s + count) {
        // 无重叠或源在目标之后，可以从前向后复制
        for (size_t i = 0; i < count; ++i) {
            d[i] = s[i];
        }
    } else {
        // 有重叠，源在目标前面，需要从后向前复制
        for (size_t i = count; i > 0; --i) {
            d[i-1] = s[i-1];
        }
    }
    return EOK; 
}

// int snprintf_s(char *strDest, size_t destMax, size_t count, const char *format, ...)
// {
//     int ret = EOK;                    /* If initialization causes  e838 */
//     va_list argList;

//     va_start(argList, format);
//     ret = vsnprintf_s(strDest, destMax, count, format, argList);
//     va_end(argList);
//     (void)argList;              /* To clear e438 last value assigned not used , the compiler will optimize this code */

//     return ret;
// }


int strncpy_s(char *dest, size_t destsz, const char *src, size_t n) {
    // 检查参数有效性
    if (dest == NULL || src == NULL || destsz == 0 || destsz > RSIZE_MAX || n > RSIZE_MAX) {
        if (dest != NULL && destsz > 0 && destsz <= RSIZE_MAX) {
            dest[0] = '\0';
        }
        return ERROR;
    }

    // 计算要复制的字符数（在n范围内查找终止符）
    size_t source_len = 0;
    while (source_len < n && src[source_len] != '\0') {
        source_len++;
    }

    // 检查目标缓冲区是否足够大
    if ((source_len == n && destsz <= n) || (source_len < n && source_len >= destsz)) {
        dest[0] = '\0';
        return ERROR;
    }

    // 执行复制并确保终止符
    for (size_t i = 0; i < source_len; i++) {
        dest[i] = src[i];
    }
    dest[source_len] = '\0';

    return 0;
}

#ifdef __cplusplus
}
#endif
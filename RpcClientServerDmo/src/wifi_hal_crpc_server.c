
#include <string.h>
#include "wifi_hal_crpc_server.h"


static WifiHalRpcFunc *g_rpcFuncHandle = NULL;

static int GetPos(const char *name)
{
    int total = 0;
    while (*name) {
        total += *name;
        ++name;
    }
    if (total < 0) {
        total *= -1;
    }
    return total % RPC_FUNC_NUM;
}
static int PushRpcFunc(const char *name, Rpcfunc func)
{
    if (g_rpcFuncHandle == NULL || name == NULL || func == NULL) {
        LOG_ERROR("%s:%d g_rpcFuncHandle is NULL", __func__, __LINE__);
        return WIFI_HAL_OPT_FAILED;
    }
    int pos = GetPos(name);
    if (g_rpcFuncHandle[pos].func == NULL) {
        strncpy_s(g_rpcFuncHandle[pos].funcname, sizeof(g_rpcFuncHandle[pos].funcname), name, strlen(name));
        g_rpcFuncHandle[pos].func = func;
    } else {
        WifiHalRpcFunc *p = g_rpcFuncHandle + pos;
        while (p->next != NULL) {
            p = p->next;
        }
        WifiHalRpcFunc *q = (WifiHalRpcFunc *)calloc(1, sizeof(WifiHalRpcFunc));
        if (q == NULL) {
            return WIFI_HAL_OPT_FAILED;
        }
        strncpy_s(q->funcname, sizeof(q->funcname), name, strlen(name));
        q->func = func;
        q->next = NULL;
        p->next = q;
    }
    return WIFI_HAL_OPT_OK;
}

int RpcStart(RpcServer *server, Context *context)
{
    LOG_INFO("%s:%d Enter", __func__, __LINE__);
    return 0;
}

int RpcStop(RpcServer *server, Context *context)
{
    LOG_INFO("%s:%d Enter", __func__, __LINE__);
    return 0;
}

int RpcStartScan(RpcServer *server, Context *context)
{
    LOG_INFO("%s:%d Enter", __func__, __LINE__);
    return 0;
}

int RpcGetScanInfos(RpcServer *server, Context *context)
{
    LOG_INFO("%s:%d Enter", __func__, __LINE__);
    return 0;
}

int InitRpcFunc(void)
{
    int ret = 0;
    if (g_rpcFuncHandle != NULL) {
        LOG_ERROR("%s:%d g_rpcFuncHandle alreadt inited", __func__, __LINE__);
        return WIFI_HAL_OPT_OK;
    }
    g_rpcFuncHandle = (WifiHalRpcFunc *)calloc(RPC_FUNC_NUM, sizeof(WifiHalRpcFunc));
    if (g_rpcFuncHandle == NULL) {
        LOG_ERROR("%s:%d calloc g_rpcFuncHandle inited", __func__, __LINE__);
        return WIFI_HAL_OPT_FAILED;
    }

    ret += PushRpcFunc("Start", RpcStart);
    ret += PushRpcFunc("Stop", RpcStop);
    ret += PushRpcFunc("StartScan", RpcStartScan);
    ret += PushRpcFunc("GetScanInfos", RpcGetScanInfos);
    return ret;

}

Rpcfunc GetRpcFunc(const char *func)
{
    if (g_rpcFuncHandle == NULL || func == NULL) {
        LOG_ERROR("%s:%d g_rpcFuncHandle or func is NULL", __func__, __LINE__);
        return NULL;
    }
    int pos = GetPos(func);
    WifiHalRpcFunc *p = g_rpcFuncHandle + pos;
    while (p && strcmp(p->funcname, func) != 0) {
        p = p->next;
    }
    if (p == NULL) {
        return NULL;
    }
    return p->func;
}

/* Processing client requests */
int OnTransactServer(RpcServer *server, Context *context)
{
    if ((server == NULL) || (context == NULL)) {
        return WIFI_HAL_OPT_FAILED;
    }

    char func[RPC_FUNCNAME_MAX_LEN] = {0};
    int ret = ReadFunc(context, func, RPC_FUNCNAME_MAX_LEN);
    if (ret < 0) {
        return WIFI_HAL_OPT_FAILED;
    }

    LOG_INFO("%s:%d run:%s", __func__, __LINE__, func);
    Rpcfunc pFunc = GetRpcFunc(func);
    if (pFunc == NULL) {
        LOG_ERROR("%s:%d unsupported function:%s ", __func__, __LINE__, func);
        WriteBegin(context, 0);
        WriteInt(context, WIFI_HAL_OPT_FAILED);
        WriteStr(context, "unsupported function");
        WriteEnd(context);
    } else {
        ret = pFunc(server, context);
        if (ret < 0) {
            WriteBegin(context, 0);
            WriteInt(context, WIFI_HAL_OPT_FAILED);
            WriteEnd(context);
        }
    }
    return WIFI_HAL_OPT_OK;
}
#include "log.h"

static log4c_category_t* g_log4c_category = NULL;
int InitLogSystem(const char *categoryName)
{
    if (log4c_init()){
        printf("%s:%d log4c_init() failed\n", __func__, __LINE__);
        return 1;
    }

    g_log4c_category = log4c_category_get(categoryName);
    if (g_log4c_category == NULL) {
        printf("%s:%d g_log4c_category is NULL\n", __func__, __LINE__);
        return 2;
    }
    printf("%s:%d log4c_init() succ\n", __func__, __LINE__);
    return 0;
}

int DeInitLogSystem()
{
    log4c_fini();
    return 0;
}

log4c_category_t* GetInitedCategory()
{
    return g_log4c_category;
}

#include <stdio.h>
#include "client.h"
#include "wifi_errno.h"
#include "log.h"

#define CONFIG_ROOR_DIR "/home/yuanling"
RpcClient *pRpcClient = NULL;

WifiErrorNo RpcClientCall(RpcClient *client, const char *func)
{
    if (client == NULL) {
        return WIFI_HAL_OPT_FAILED;
    }
    int ret = RemoteCall(client);
    if (ret < 0) {
        LOG_ERROR("%s:%d RemoteCall fail:%d \n", __func__, __LINE__, ret);
        UnlockRpcClient(client);
        return WIFI_HAL_OPT_FAILED;
    }

    return WIFI_HAL_OPT_OK;
}


WifiErrorNo Start(void)
{
    // RpcClient *client = GetStaRpcClient();
    RpcClient *client = pRpcClient;
    LockRpcClient(client);
    Context *context = client->context;
    WriteBegin(context, 0);
    WriteFunc(context, "Start");
    WriteEnd(context);

    if (RpcClientCall(client, "Start") != WIFI_HAL_OPT_OK) {
        LOG_ERROR("%s:%d RpcClientCall Fail\n", __func__, __LINE__);
        return WIFI_HAL_OPT_FAILED;
    }
    int result = WIFI_HAL_OPT_FAILED;
    ReadInt(context, &result);
    ReadClientEnd(client);
    UnlockRpcClient(client);
    return result;
}

WifiErrorNo StartScan()
{
    RpcClient *client = pRpcClient;
    LockRpcClient(client);
    Context *context = client->context;
    WriteBegin(context, 0);
    WriteFunc(context, "StartScan");
    WriteEnd(context);
    if (RpcClientCall(client, "StartScan") != WIFI_HAL_OPT_OK) {
        return WIFI_HAL_OPT_FAILED;
    }
    int result = WIFI_HAL_OPT_FAILED;
    ReadInt(context, &result);
    ReadClientEnd(client);
    UnlockRpcClient(client);
    return result;
}

WifiErrorNo Stop()
{
    RpcClient *client = pRpcClient;
    LockRpcClient(client);
    Context *context = client->context;
    WriteBegin(context, 0);
    WriteFunc(context, "Stop");
    WriteEnd(context);
    if (RpcClientCall(client, "Stop") != WIFI_HAL_OPT_OK) {
        return WIFI_HAL_OPT_FAILED;
    }
    int result = WIFI_HAL_OPT_FAILED;
    ReadInt(context, &result);
    ReadClientEnd(client);
    UnlockRpcClient(client);
    return result;
}

int main(void)
{
    char idlSockPath[] = CONFIG_ROOR_DIR"/unix_sock.sock";
    int i = 0;

    InitLogSystem("rpc_client_log");
    pRpcClient = CreateRpcClient(idlSockPath);
    if (pRpcClient == NULL) {
         LOG_ERROR("%s:%d CreateRpcClient Fail", __func__, __LINE__);
         DeInitLogSystem();
         return -1;
     }
     LOG_INFO("%s:%d CreateRpcClient Succ", __func__, __LINE__);
     sleep(3);
     Start();
     sleep(1);
     StartScan();
     sleep(1);
     Stop();
    while(i < 10) {
        LOG_INFO("%s:%d %d", __func__, __LINE__, i);
        i++;
    }
    ReleaseRpcClient(pRpcClient);
    DeInitLogSystem();

    return 0;
}
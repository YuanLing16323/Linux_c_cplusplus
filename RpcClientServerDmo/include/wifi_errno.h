#ifndef WIFI_ERRNO_H
#define WIFI_ERRNO_H

typedef enum WifiErrorNo {
    WIFI_HAL_OPT_OK = 0,                     /* Hal operation succeeded */
    WIFI_HAL_OPT_FAILED = 1,                 /* Hal operation failed */
    WIFI_HAL_OPT_SCAN_BUSY = 2,              /* Scan failed. Scan busy. */
    WIFI_HAL_OPT_PBC_OVERLAP = 3,            /* WPS PBC mode: overlap */
    WIFI_HAL_OPT_SUPPLICANT_NOT_INIT = 4,    /* The wpa_supplicant is not initialized or fails to be initialized */
    WIFI_HAL_OPT_OPEN_SUPPLICANT_FAILED = 5, /* Failed to enable wpa_supplicant. */
    WIFI_HAL_OPT_CONN_SUPPLICANT_FAILED = 6, /* Failed to connect to wpa_supplicant. */
    WIFI_HAL_OPT_HOSTAPD_NOT_INIT = 7,       /* Hostapd is not initialized or initialization fails. */
    WIFI_HAL_OPT_OPEN_HOSTAPD_FAILED = 8,    /* Failed to start the hostapd. */
    WIFI_HAL_OPT_CONN_HOSTAPD_FAILED = 9,    /* Failed to connect to the hostapd. */
    WIFI_HAL_OPT_NOT_SUPPORT,
    WIFI_HAL_OPT_GET_WIFI_COND_FAILED,
    WIFI_HAL_OPT_BUFFER_TOO_LITTLE,
    WIFI_HAL_OPT_INPUT_MAC_INVALID,
    WIFI_HAL_OPT_GET_VENDOR_HAL_FAILED,         /* Initialized vendor hal failed. */
    WIFI_HAL_OPT_VENDOR_UNKNOWN,
    WIFI_HAL_OPT_VENDOR_UNINITIALIZED,
    WIFI_HAL_OPT_VENDOR_NOT_AVAILABLE,
    WIFI_HAL_OPT_VENDOR_INVALID_ARGS,
    WIFI_HAL_OPT_VENDOR_INVALID_REQUEST_ID,
    WIFI_HAL_OPT_VENDOR_TIMED_OUT,
    WIFI_HAL_OPT_VENDOR_TOO_MANY_REQUESTS,
    WIFI_HAL_OPT_VENDOR_OUT_OF_MEMORY,
    WIFI_HAL_OPT_VENDOR_BUSY,
    WIFI_HAL_OPT_INVALID_PARAM,
    WIFI_HAL_OPT_GET_P2P_GROUP_INFACE_FAILED,
} WifiErrorNo;



// typedef enum WifiErrorNo {
//     WIFI_HAL_SUCCESS = 0,                /* Success. */
//     WIFI_HAL_FAILED = 1,                 /* Failed. */
//     WIFI_HAL_SCAN_BUSY = 2,              /* Scan failed. Scan busy. */
//     WIFI_HAL_PBC_OVERLAP = 3,            /* WPS PBC mode overlap. */
//     WIFI_HAL_SUPPLICANT_NOT_INIT = 4,    /* The wpa_supplicant is not initialized or fails to be initialized. */
//     WIFI_HAL_OPEN_SUPPLICANT_FAILED = 5, /* Start wpa_supplicant failed. */
//     WIFI_HAL_CONN_SUPPLICANT_FAILED = 6, /* Connect wpa_supplicant failed. */
//     WIFI_HAL_HOSTAPD_NOT_INIT = 7,       /* Hostapd is not initialized or initialization fails. */
//     WIFI_HAL_OPEN_HOSTAPD_FAILED = 8,    /* Start hostapd failed. */
//     WIFI_HAL_CONN_HOSTAPD_FAILED = 9,    /* Connect hostapd failed. */
//     WIFI_HAL_NOT_SUPPORT = 10,           /* Not supported currently. */
//     WIFI_HAL_GET_WIFI_COND_FAILED,       /* Initialized  wificond failed. */
//     WIFI_HAL_BUFFER_TOO_LITTLE,          /* request buffer size too small */
//     WIFI_HAL_INPUT_MAC_INVALID,
//     WIFI_HAL_GET_VENDOR_HAL_FAILED, /* Initialized vendor hal failed. */
//     WIFI_HAL_VENDOR_UNKNOWN,
//     WIFI_HAL_VENDOR_UNINITIALIZED,
//     WIFI_HAL_VENDOR_NOT_AVAILABLE,
//     WIFI_HAL_VENDOR_INVALID_ARGS,
//     WIFI_HAL_VENDOR_INVALID_REQUEST_ID,
//     WIFI_HAL_VENDOR_TIMED_OUT,
//     WIFI_HAL_VENDOR_TOO_MANY_REQUESTS,
//     WIFI_HAL_VENDOR_OUT_OF_MEMORY,
//     WIFI_HAL_VENDOR_BUSY,
//     WIFI_HAL_INVALID_PARAM,
//     WIFI_HAL_GET_P2P_GROUP_INFACE_FAILED,
// } WifiErrorNo;

#endif
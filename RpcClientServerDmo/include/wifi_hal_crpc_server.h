#ifndef WIFI_HAL_CRPC_SERVER_H
#define WIFI_HAL_CRPC_SERVER_H

#include "wifi_errno.h"
#include "server.h" /* RPC Server header file */
#include "log.h"

#define RPC_FUNC_NUM 10
#define RPC_FUNCNAME_MAX_LEN 128
#define WIFI_COM_STR_LENGTH 512

typedef int (*Rpcfunc)(RpcServer *server, Context *context);

typedef struct WifiHalRpcFunc {
    char funcname[128];
    Rpcfunc func;
    struct WifiHalRpcFunc *next;
} WifiHalRpcFunc;

/**
 * @Description Initialization the function table.
 *
 * @return int - 0 Success, -1 Failed.
 */
int InitRpcFunc(void);
/**
 * @Description Release the function table.
 *
 */
void ReleaseRpcFunc(void);
/**
 * @Description Get the Rpc Func object.
 *
 * @param func - Function name string.
 * @return Rpcfunc - Function pointer found.
 */
Rpcfunc GetRpcFunc(const char *func);

int OnTransactServer(RpcServer *server, Context *context);


#endif
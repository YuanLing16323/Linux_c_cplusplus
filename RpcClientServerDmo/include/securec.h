#ifndef CRPC_EVENT_LOOP_H
#define CRPC_EVENT_LOOP_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stddef.h>
#include <stdio.h>
#define ERROR 1
#define EOK 0

#define RSIZE_MAX (1024 * 1024 * 32)

int memset_s(void *dest, size_t destsz, int ch, size_t count);
int memmove_s(void *dest, size_t destsz, const void *src, size_t count);

// int snprintf_s(char *strDest, size_t destMax, size_t count, const char *format, ...);

int strncpy_s(char *dest, size_t destsz, const char *src, size_t n);

#ifdef __cplusplus
}
#endif
#endif
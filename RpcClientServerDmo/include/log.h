/*
 * Copyright (C) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CPRC_WIFI_LOG_H
#define CPRC_WIFI_LOG_H

#include "log4c.h"

int InitLogSystem(const char *categoryName);
int DeInitLogSystem();

log4c_category_t* GetInitedCategory();

#define LOG_ERROR(msg, args...) \
    { \
        const log4c_location_info_t locinfo = LOG4C_LOCATION_INFO_INITIALIZER(NULL);\
        log4c_category_log_locinfo(GetInitedCategory(), &locinfo, LOG4C_PRIORITY_ERROR, msg, ##args); \
    }

#define LOG_WARN(msg, args...) \
    { \
        const log4c_location_info_t locinfo = LOG4C_LOCATION_INFO_INITIALIZER(NULL);\
        log4c_category_log_locinfo(GetInitedCategory(), &locinfo, LOG4C_PRIORITY_WARN, msg, ##args); \
    } 

#define LOG_INFO(msg, args...) \
    { \
        const log4c_location_info_t locinfo = LOG4C_LOCATION_INFO_INITIALIZER(NULL); \
        log4c_category_log_locinfo(GetInitedCategory(), &locinfo, LOG4C_PRIORITY_INFO, msg, ##args); \
    }

#define LOG_DEBUG(msg, args...) \
    { \
        const log4c_location_info_t locinfo = LOG4C_LOCATION_INFO_INITIALIZER(NULL); \
        log4c_category_log_locinfo(GetInitedCategory(), &locinfo, LOG4C_PRIORITY_DEBUG, msg, ##args); \
    }

#define LOG_TRACE(msg, args...) \
    { \
        const log4c_location_info_t locinfo = LOG4C_LOCATION_INFO_INITIALIZER(NULL);\
        log4c_category_log_locinfo(GetInitedCategory(), &locinfo, LOG4C_PRIORITY_TRACE, msg, ##args); \
    }

#endif
1.snprintf：将可变个参数(...)按照format格式化成字符串，然后将其复制到str中。
	原型：int snprintf(char *str, size_t size, const char *format, ...)
	返回值：若成功则返回预写入的字符串长度，若出错则返回负值。

	(1) 如果格式化后的字符串长度 < size，则将此字符串全部复制到str中，
		并给其后添加一个字符串结束符('\0')；
		
	(2) 如果格式化后的字符串长度 >= size，则只将其中的(size-1)个字符复制到str中，
		并给其后添加一个字符串结束符('\0')。
		
		
2.strstr：
	#include <string.h> --c语言的头文件
	#include <cstring>  --c++语言的头文件
	
	char* strstr(const char* str1,const char* str2);
	
	strstr函数是用于判断一个字符串内部是否包含另一个字符串。（'\0'不计算于内）
	返回类型：
		当str1指针指向的字符串包含str2指针指向的字符串时，返回str1指针指向的字符串中str2指向的字符串的起始位地址。

		例如：abcdef字符串中包含cde字符串用该函数会返回abcdef字符串中c的地址。

		当str1指针指向的字符串不包含str2指针指向的字符串，返回一个空指针即NULL。

3.memmove函数
	
	void *memmove(
	   void *dest,
	   const void *src,
	   size_t count 
	);
	参数：
		dest：指向存放复制内容的内存空间首地址的指针；
		src：指向被复制内容的内存空间首地址的指针；
		count：复制内容的最大字节数。

	函数作用：
		将src中的内容按字节挨个复制到dest中。

	返回值：
		返回一个指向目标存储区dest的指针。


	memmove_s:

		errno_t memmove_s(
		   void *dest,
		   size_t numberOfElements,
		   const void *src,
		   size_t count
		);
		参数：
			dest：指向存放复制内容的内存空间首地址的指针；
			numberOfElements：dest所指内存空间的缓冲区大小，实际复制的字节数上限；
			src：指向被复制内容的内存空间首地址的指针；
			count：复制内容的期望字节数大小。

		函数作用：
			由src所指内存区域复制不多于count个字节到dest所指内存区域，但是count不能大于numberOfElements。这样可以确保dest所指内存空间的数据安全。

		返回值：
			一个整型值（errno_t），用来判断复制是否完成。0表示成功。

	memmove函数与memmove_s的区别：
		当复制的内存空间与被复制的内存空间发生局部重叠时，memmove_s可以保证拷贝内容正确并成功覆盖。













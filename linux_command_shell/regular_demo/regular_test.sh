#!/bin/bash

# 定位符==========================================================================================
# ---从头开始 ^

# echo "The bool stor" | sed -n '/^book/p'
# echo "Books are great" | sed -n '/^Book/p'

#---查找结尾 $
# echo "This is a good book" | sed - n '/book$/p'
# echo "This book is good " | sed - n '/book$/p'

#---联合定位 

    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ cat data1.txt
    # this is a test of using both anchors
    # I said this is a test
    #
    # this is a test
    #
    # I'm sure this is a test.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$

#sed -n '/^this is a test$/p' data1.txt # 输出：this is a test

# sed '/^$/d' data1.txt   # 匹配行开始和结尾之间没有任何内容的行。
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ ./regular_test.sh
    # this is a test of using both anchors
    # I said this is a test
    # this is a test
    # I'm sure this is a test.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$


# 点字符============================================================================================
# 点特殊字符用于匹配除换行符之外的任何单个字符。但点字符必须匹配一个字符；
# 如果在点字符位置没有字符，那么模式匹配失败
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ cat data2_dot.txt
    # this is a test of a line.
    # The cat is sleeping.
    # That is very nice hat.
    # This test is at line four.
    # at ten o'clock we'll go home.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$

# sed -n '/.at/p' data2_dot.txt
#     yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ ./regular_test.sh
#     The cat is sleeping.
#     That is very nice hat.
#     This test is at line four.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$

# 字符类============================================================================================
# ----[]

# sed -n '/[ch]at/p' data2_dot.txt # 匹配 cat和hat

#     yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ ./regular_test.sh
#     The cat is sleeping.
#     That is very nice hat.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$

# echo "Yes" | sed -n '/[Yy]es/p'
# echo "yes" | sed -n '/[Yy]es/p'

# echo "yEs" | sed -n '/[Yy][Ee][Ss]/p'
# echo "yes" | sed -n '/[Yy][Ee][Ss]/p'
# echo "yeS" | sed -n '/[Yy][Ee][Ss]/p'

# ---[^ ] 否定字符类 匹配不再字符类中的模式
# sed -n '/[^ch]at/p' data2_dot.txt

# ---使用范围
# sed -n '^[0123456789][0123456789][0123456789][0123456789][0123456789]$/p'
# #使用范围
# sed -n '^[0-9][0-9][0-9][0-9]0-9]$/p'

# sed -n '/[a-ch-m]at/p' data2_dot.txt
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$ ./regular_test.sh
    # The cat is sleeping.
    # That is very nice hat.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/regular_demo$



# 星号============================================================================================
# 在某个字符之后加一个* 表示该字符可以出现>=0 次

# ie*k 匹配：ik iek ieek ieeek

# b[ae]*t 匹配：bt bat bet btt baat baaeeet baeeaeeat 
# 		但不匹配baabeeet





# ==================================================================================================
# --------扩展的正则表达式gawk能识别，sed不能识别
# ==================================================================================================

# 问号==============================================================================================
# 在某个字符之后加一个？ 表示该字符可以不出现或者出现1次

# be?t 匹配：bt bet 不匹配beet beeet

# b[ae]?t 匹配：bt bat bet. 不匹配bot baet  beat beet
echo "bat" | gawk '/b[ae]?t/{print $0}'

# + 加号 ===========================================================================================
# 加号表示其前面的字符至少出现1次

# be+t 匹配: beeet beet  bet  不匹配bt
# b[ae]t 匹配：bat bet beat beeat 不匹配 bt

# echo "bt" | gawk --re-interval '/be{1}t/{print $0}'
# echo "bet" | gawk --re-interval '/be{1}t/{print $0}' # 只有这个匹配
# echo "beet" | gawk --re-interval '/be{1}t/{print $0}'

# () 分组============================================================================================

# sed 's,\(.*\)\.o[ :]*,objs/\1.o: ,g' < filename
# 分隔符：使用逗号,代替默认的/，避免转义干扰。

# s命令格式：s/模式/替换内容/选项，这里用逗号分隔。

# 全局替换：末尾的g表示替换所有匹配项（但实际受正则影响，可能仅替换一次）。

# 正则表达式解析
# \(.*\)\.o[ :]*

# 捕获组\(.*\)：

# .*匹配任意字符（贪婪模式），括号将其捕获为\1。

# 例如：main从main.o中被捕获。

# 字面匹配.o：

# \.o匹配字面的.o（转义.避免通配）。

# 后缀匹配[ :]*

# 匹配.o后的空格或冒号（0次或多次），如: 或空格。


# echo  "bar.o: bar.c \\" | sed 's,\(.*\)\.o[ :]*,objs/\1.o: , g'

# echo "cab" | sed -n 's/\(^c\|b\)/\1xxx/p' 







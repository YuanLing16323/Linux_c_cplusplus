#!/bin/bash
# 脚本第一行一定要是 #!/bin/bash 指示运行该脚本的shell


# 1. 注释===============================================================================
#  "#" 号是注释。但是脚本第一行一定要是 #!/bin/bash 指示运行该脚本的shell


# 2 echo 显示消息=======================================================================
# ---eg 1 默认情况下不需要使用引号来标记内容----------------------------------------------
    # echo The time and date are:
    # date
    # 输出:
    # yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
    # The time and date are:
    # Tue Feb 25 18:28:26 CST 2025

# ---eg 2 使用单引号或者双引号来标记显示内容----------------------------------------------
    # echo "Hello, welcome!"
    # echo 'How are you? are you ok'
    # echo "This is a test to see if you're paying attention"
    # echo 'Rich says "scripting is easy".'
    # 输出:
        # yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
        # Hello, welcome!
        # How are you? are you ok
        # This is a test to see if you're paying attention
        # Rich says "scripting is easy".
        # yuanling@ISS:~/stduy_demo/command_shell_demo$
# ---eg 3 -n参数------------------------------------------------------------------------
#-n 参数使echo命令结果和echo文本字符串在同一行
    # echo "This time and date are:"
    # date

    # echo -n "This time and date are:"
    # date
    # 输出：
        # yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
        # This time and date are:
        # Fri Feb 28 08:47:07 CST 2025
        # This time and date are:Fri Feb 28 08:47:07 CST 2025
        # yuanling@ISS:~/stduy_demo/command_shell_demo$




# 3.使用变量=============================================================================
    # ---part 1.环境变量---------------------------------------------------------------------
        # set #显示环境变量

        # 可以使用$号来引用这些环境变量
            # echo "User info for userid: ${USER}"
            # echo UID: ${UID}
            # echo HOME: ${HOME}
            # 输出：
                # yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
                # User info for userid: yuanling
                # UID: 1000
                # HOME: /home/yuanling

    # ---part 2.自定义变量-------------------------------------------------------------------
    # var1=10 # 在变量、等号和值之间不能有空格！！！！
    # var2=3.14
    # var3="This is a string"
    # echo $var1
    # echo $var2
    # echo $var3

    # var4=${var3}
    # echo ${var4}

# 4.反引号=====================================================================================
    # 反引号与~在同一个建上。
    # 反引号运行将shell命令的输出赋值给变量。
    # data_str=`date`
    # echo ${data_str}
    # today=`date +%y%m%d`
    # logfilename=log.${today}
    # echo ${logfilename}

# 5.重定向输入与输出=============================================================================
    # ---part 1.输出重定向----------------------------------------------------------------------
        #  输出重定向符是 >
        # ls > lsout 
        # ls -l >> lsout #附加到现有文件，而不是重写

    # ---part 2.输入重定向----------------------------------------------------------------------
        #  输入重定向符是 <



# 6.管道=======================================================================================
    # 管道的符号是  |  作用是 将一个命令的输出发送至另一个命令作为输入
    # ls -l | wc


# 7.数学计算====================================================================================
    # ---part 1.expr命令----------------------------------------------------------------------
        # expr 1+5 # 会输出 1+5
        # expr 1 + 5 #会输出 6
        # res=`expr 1 + 5`
        # echo $res

        # expr 支持 加减乘除求余  逻辑运算：大于 小于 等于 不等于 大于等于 小于等于 (返回0或者1)
        #  ARG1 | ARG2 如果两个参数都不为空返回ARG1； 如果有一个不为空，就返回它。 否则返回0
        #  ARG1 & ARG2 如果两个参数都不为空.返回ARG1； 否则返回0
        # 使用特殊字符（如 <, >, (, ), |, &）时要进行转义或用引号括起来。
        # res=`expr 6 \& 0` # 0
        # res1=`expr 7 \& 8` # 7
        # res2=`expr 5 \| 2` # 5
        # res3=`expr 0 \| 3` # 3
        # res4=`expr 9 \| 0` # 9
        # res5=`expr false \| 0` # false
        # echo ${res}
        # echo ${res1}
        # echo ${res2}
        # echo ${res3}
        # echo ${res4}
        # echo ${res5}
    # ---part 2.使用[]--------------------------------------------------------------------------
        # var0=$[1 + 5]
        # var1=$[${var0} * 2]
        # echo ${var0}
        # echo ${var1}
    # ---part 3.浮点解决方案----bc-----------------------------------------------------------------
    # var1=`echo "scale=4; 3.44/5" | bc`  # 这里使用 scale变量设置小数点位数为4
    # echo $var1 # 输出 .6880 

# 8.退出脚本=======================================================================================
    # ---part 1.核对退出状态---$?-------------------------------------------------------------------
        # $?保存最后一个命令的退出状态 如果想核对一条命令的退出状态，必须在这条命令完成之后立即查看或使用变量$?
        # 一条命令成功完成的退出状态是0.非零表示错误
            # 0 命令成功完成
            # 1 通常的未知错误，如命令的参数错误
            # 126 命令无法执行，通常表明用户没有权限执行这条命令
            # 127 没有找到该命令
     # ---part 2.退出命令--exit-----------------------------------------------------------------------
    #  echo "Hello world"
    #  exit 5

    #     yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
    #     Hello world
    #     yuanling@ISS:~/stduy_demo/command_shell_demo$ echo $?
    #     0
    #     yuanling@ISS:~/stduy_demo/command_shell_demo$ ./shell_part1.sh
    #     Hello world
    #     yuanling@ISS:~/stduy_demo/command_shell_demo$ echo $?
    #     5
    #     yuanling@ISS:~/stduy_demo/command_shell_demo$


        
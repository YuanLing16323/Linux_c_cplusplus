#!/bin/bash

# sed 's,\(.*\)\.o[ :]*,objs/\1.o bar.dep: ,g' < bar.dep.tmp > bar.dep


# s 命令=======================================================================================
# 这里使用 / 和 \都一样。但是要统一
# yuanling@ISS:~$ echo "This is a test" | sed 's/test/big/'
# This is a big
# yuanling@ISS:~$

# s命令。s命令用第二个文本字符串替换第一个两个斜杠之间指定的文本字符串。
# 本丽中，用单词big 替换了test


# yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ cat data1
# This quick brown fox jumos over the lazy dog.
# This quick brown fox jumos over the lazy dog.
# This quick brown fox jumos over the lazy dog.
# This quick brown fox jumos over the lazy dog.
# yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$
# 有没有 < 都一样！！！！ 将单词dog替换为cat
#sed 's/dog/cat/' < data1  
# sed 's/dog/cat/' data1


# -e参数  一次执行多个命令 =========================================================================
# 命令必须用 ; 分开， 且在命令结尾和; 之间不能有任何空格
#sed -e 's/dog/cat/; s/brown/green/' < data1 


# 标记替换=========================================================================================

#echo "This is test of the test script" | sed 's\test\trial\' # This is trial of the test script
#第二个 单词test没有替换

#默认情况下仅替换各行中首次出现的文本 要使替换命令继续替换之后出现的问题，则必须使用替换标记！

# s/pattern/replacement/flags
# 可用的替换标记有4种
# * 数字：表示替换第几个！
# * g: 替换所有
# * p: 表示打印原始行的内容
# * w file: 将替换的结果写入文件
#echo "This is test of the test script" | sed 's\test\trial\2'  # This is test of the trial script
#echo "This is test of the test script" | sed 's\test\trial\g'  # This is trial of the trial script
#echo "This is test of the test script" | sed 's\test\trial\gw result.txt'

# -n 和 p结合使用，只显示匹配到的行
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ cat data5
    # This is a test line.
    # This is a different Line
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

#sed -n "s/test/trial/p" < data5
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
    # This is a trial line.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# 定界符也可以是 ! 或者,
# echo "This is test of the test script" | sed 's!test!trial!2'  # This is test of the trial script
# echo "This is test of the test script" | sed 's,test,trial,2'


#使用地址===========================================================================================
#数字式行寻址---------------------------------------------------------------------------------
#sed编辑器指定行号从1开始。命令可以指定的地址可以是单个行号，也可以是[起始行号,结束行号]范围

# sed '2s/dog/cat/' < data1  # 只处理第2行
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy cat.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '2,4s/dog/cat/' < data1  # 只处理第2,3,4 共3行
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy cat.
#     This quick brown fox jumos over the lazy cat.
#     This quick brown fox jumos over the lazy cat.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '3,$s/dog/cat/' < data1 # $符号的含义，处理从某一行开始到最后
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy cat.
#     This quick brown fox jumos over the lazy cat.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

#使用文本模式筛选器-------------------------------------------------------------------------
# to do


#组合命令----------------------------------------------------------------------------------
#如果需要再单独一行上执行多个命令，请使用大括号将命令组合在一起。
# 直接在shell里输入是可以的
# yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ sed '2{
# > s/fox/elephant/
# > s/dog/cat/
# > }' data1
    # This quick brown fox jumos over the lazy dog.
    # This quick brown elephant jumos over the lazy cat.
    # This quick brown fox jumos over the lazy dog.
    # This quick brown fox jumos over the lazy dog.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# 如果要写入脚本呢？
# sed '2{s/fox/elephant/;s/dot/cat/}' data1 并没有对dog进行替换！！！

    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
    # This quick brown fox jumos over the lazy dog.
    # This quick brown elephant jumos over the lazy dog.
    # This quick brown fox jumos over the lazy dog.
    # This quick brown fox jumos over the lazy dog.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$
    


#删除行===========================================================================================
# 命令d完成删除功能

# yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ cat data6
# This is Line number 1
# This is Line number 2
# This is Line number 3
# This is Line number 4
# This is Line number 5
# This is Line number 6
# This begin
# aaa
# bbb
# ccc
# This end
# ddd
# yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

#sed '2d' data6 #删除指定行
#sed '2,3d' data6  #删除2,3两行 (删除范围)
#sed '/number 1/d' data6 #删除有包含number的行
#sed '/begin/, /end/d' data6  # 删除匹配行和这两行中间的行  如果第二个没有匹配到。则删除第一个匹配到的行后面的所有行



#插入和附加文本===========================================================================================

# 插入命令 i 在指定行之前插入一行
# 附加命令 a 在指定行之后加入一行

#sed '[address]command newline'

# echo "testing" | sed 'i This is a test' # 前面插入
# echo "testing" | sed 'a This is a test' # 后面插入

#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This is a test
#     testing
#     testing
#     This is a test
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

#sed '3i This is insertline' < data1  # 在第3行前添加一行
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This is insertline
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '3a This is insertline' < data1  # 在第3行后添加一行
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This is insertline
#     This quick brown fox jumos over the lazy dog.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '$a This is insert line' < data1 # 在最后一行后添加一行
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This is insert line
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '1i This is insert line' < data1 # 在在第1行添加
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This is insert line
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$


#更改行===========================================================================================
# c命令用于更改行
# sed '3c This a modified line' < data1  # 即用新内容替换第3行

#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This quick brown fox jumos over the lazy dog.
#     This quick brown fox jumos over the lazy dog.
#     This a modified line
#     This quick brown fox jumos over the lazy dog.
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

#sed '/fox/c hahahahahah' < data1  # 替换所有匹配的行

#变换命令===========================================================================================
# 变换命令 y 是唯一一个对单个字符操作的命令 格式为：
# [address]y/inchars/outchars/

# inchars和outchars长度必须相同，否则sed将生成错误

# sed 'y/123/789/' data6
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
#     This is Line number 7
#     This is Line number 8
#     This is Line number 9
#     This is Line number 4
#     This is Line number 5
#     This is Line number 6
#     This begin
#     aaa
#     bbb
#     ccc
#     This end
#     ddd
#     yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$
# 变换命令是全局命令。也就是说它替换所有的。
# echo "This 1 is a test of 1 try." | sed 'y/123/456/'
# 输出:This 4 is a test of 4 try.


#写文件命令===========================================================================================
# w命令用于写文件


#=======================================sed 高级编程 =================================================
# N 命令

    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ cat data_n
    # This is the header line.
    # This is the first data line.
    # This is the second data line.
    # This is the last line.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# sed '/first/{N;s/\n/ /}' data_n 
    # 解析
    # 搜索包含单词first的文本行。当找到该行时，使用N命令将该行与下一行合并，
    # 然后替换命令(s)将换行符替换为一个空格。结果就是两行显示为一行

    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$ ./test_sed.sh
    # This is the header line.
    # This is the first data line. This is the second data line.
    # This is the last line.
    # yuanling@ISS:~/stduy_demo/command_shell_demo/sed_sample$

# bar.o: bar.c \
#  /home/yuanling/stduy_demo/makefile_demo/huge/code/bar/inc/bar.h

# objs/bar.o deps/bar.dep: bar.c \
#  /home/yuanling/stduy_demo/makefile_demo/huge/code/bar/inc/bar.h





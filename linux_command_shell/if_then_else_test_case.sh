#!/bin/bash

# 1.if-then语句=====================================================================================
    # bash shell中的if语句运行在if行定义的命令。如果命令的退出状态是0(命令成功执行)，
    # 将执行then后面的所有命令。如果命令的退出状态是0意外的任何值，那么then后面的指令将不会执行

    # if date
    # then
    #     echo "cmd date worked"
    # fi

    # 可以将then和if放在同一行，但是要用;隔开
    # if date; then
    #     echo "cmd date worked"
    # fi

# 2.if-then-else语句=====================================================================================
    # if asdfg
    # then
    #     echo "it didn't work"
    # else
    #     echo "cmd asdfg error"
    # fi

# 3.嵌套if语句 if-then-elif-fi============================================================================
    # if cmd1
    # then
    #     cmds
    # elif cmd2
    # then
    #     other cmds
    # fi


# 4.test命令==============================================================================================
    # 如果test命令中列出的条件评估值为true，test命令以0退出状态码退出。

    # 格式为：
        # if test condition
        # then
        #     commands
        # fi
    # bash shell 提供另一种在if-then语句中声明test的方法：
        # if [ condition ] # 注意 在前半个方括号的后面和后半个方括号的签名必须都有一个空格！！！！
        # then
        #     commands
        # fi

    # test命令能够评估一下3类条件
    #     数值比较
    #     字符串比较
    #     文本比较

    # ---part 1.数值比较---------------------------------------------------------------------------------
        # n1 -eq n2 检查 n1 == n2
        # n1 -ge n2 检查 n1 >= n2
        # n1 -gt n2 检查 n1 >  n2
        # n1 -le n2 检查 n1 <= n2
        # n1 -lt n2 检查 n1 <  n2
        # n1 -ne n2 检查 n1 != n2

        # val1=10
        # val2=11

        # if [ $val1 -gt 5 ]
        # then
        #     echo "The test value $val1 is greater than 5"
        # fi

        # if [ $val1 -eq $val2 ]
        # then
        #     echo "The test value $val1 equal to $val2"
        # else
        #     echo "The test value $val1 are different to $val2"
        # fi

        # test命令不能处理浮点数!!!!!!!!!!!
        # var3=3.14
        # if [ $val1 -gt 3 ]
        # then
        #     echo "The test value $val1 is greater than 3"
        # fi

    # ---part 2.字符串比较-------------------------------------------------------------------------------
        # str1 = str2   是否相等
        # str1 != str2  是否不同
        # str1 < str2   str1是否小于str2
        # str1 > str2   str1是否大于str2
        # -n str1       str1的长度是否大于0
        # -z str1       str1的长度是否为0
        
        # str1="abcde"
        # str2="abcde"
        # str3="zxcvb"
        # str4=""

        # echo "str1 = ${str1}"
        # echo "str2 = ${str2}"
        # echo "str3 = ${str3}"
        # echo "str4 = ${str4}"

        # if [ $str1 = $str2 ]; then
        #     echo "str1 is equal str2"
        # fi

        # if [ $str1 != $str3 ]; then
        #     echo "str1 is different to str3"
        # fi

        # if [ $str1 \< $str3 ]; then   # 使用 < > 要进行转义
        #     echo "str1 < str3"
        # fi

        # if [ -n $str3 ]; then
        #     echo "str3 len is > 0"
        # fi

        # if [ -z $str4 ]; then
        #     echo "str4 len is  0"
        # fi

    # ---part 3.文件比较-------------------------------------------------------------------------------
        # test命令能够测试Linux文件系统上的文件状态和路径

        # -d file    检查file是否纯在并且是一个目录
        # -f file    检查file是否纯在并且是一个文件
        # -e file    检查file是否纯在 

        # -r file    检查file是否存在并且可读
        # -w file    检查file是否存在并且可写
        # -x file    检查file是否存在并且可执行
        # -s file    检查file是否存在并且不为空

        # -O file    检查file是否存在并且被当前用户拥有
        # -G file    检查file是否存在并且默认组是否为当前用户
        # file1 -nt file2  检查file1是否比file2新
        # file1 -ot file2  检查file1是否比file2旧

        # echo $HOME
        # if [ -d ${HOME} ]; then
        #     echo "${HOME} is a dir"
        # fi

        # if [ -f ${HOME} ]; then
        #     echo "${HOME} is a file"
        # else
        #     echo "${HOME} is not a file"
        # fi

        # if [ -e ${HOME} ]; then
        #     echo "${HOME} is exist"
        # else
        #     echo "${HOME} is not exist"
        # fi



# 5.复合条件检查==============================================================================================







